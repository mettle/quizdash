(function() {
    'use strict';

    angular
        .module('app.privacy')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'anon.privacy',
                config: {
                    url: '/privacy',
                    views: {
                        'content@': {
                            templateUrl: '/views/privacy/privacy.html',
                            controller: 'Privacy',
                            controllerAs: 'vm'
                        },
                        'circles': {
                            templateUrl: '/views/footer/circles.html'
                        }
                    }
                }
            }
        ];
    }
})();