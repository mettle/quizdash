/* File: gulpfile.js */

// grab our packages
var autoprefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'),
	gulp   = require('gulp'),
	gutil  = require('gulp-util'),
  mainBowerFiles = require('main-bower-files'),
  sass = require('gulp-sass'),
  rename = require('gulp-rename'),
  minifyCss = require('gulp-minify-css'),
  sourcemaps = require('gulp-sourcemaps');

// concatenate all application javascript into 'public'
gulp.task('app', function() {
	var files = [
		'source/app/**/*.module.js',
		'source/app/**/*.route.js',
		'source/app/**/*.js'
	];
	return gulp.src(files)
		.pipe(concat('app.js'))
		.pipe(gulp.dest('public/js'));
});

// default task to output message
gulp.task('default', function() {
	return gutil.log('Gulp is running!')
});

// concatenate and minify .scss files
gulp.task('styles', function() {
	return gulp.src('source/styles/*.scss')
    .pipe(sourcemaps.init())
		.pipe(sass({onError: function(e) { console.log(e); } }))
		.pipe(autoprefixer("last 2 versions", "> 1%", "ie 8"))
		.pipe(concat('styles.css'))
		.pipe(minifyCss())
    	.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('public/css'));
});

// move images to 'dist'
gulp.task('images', function() {
	files = [
		'source/images/*.png',
		'source/images/*.jpg',
		'source/favicon.ico'
	]
	gulp.src(files)
		.pipe(gulp.dest('public/images'));
});

// concatenate all vendor files into 'public'
gulp.task('vendor', function() {
	gulp.src(mainBowerFiles({ paths: './source' }))
		.pipe(concat('vendor.js'))
		.pipe(gulp.dest('public/js'));
	gulp.src('source/bower_components/angular-toastr/dist/angular-toastr.min.css')
		.pipe(concat('vendor.css'))
		.pipe(gulp.dest('public/css'));
});

// add all html into 'public'
gulp.task('views', function() {
	gulp.src('source/index.html')
  	.pipe(rename("angular.html"))
		.pipe(gulp.dest('public'));

	gulp.src('source/app/**/*.html')
		.pipe(gulp.dest('public/views'));
});

gulp.task('dist', function() {
  files = ['**/*', '!.git*', '!.git/', '!.git/*', '!.git/**/*', '!.env*', '!public/assets', '!public/assets/*', '!public/blog', '!public/blog/*', '!public/blog/**', '!source', '!source/*', '!source/**/*', '!node_modules', '!node_modules/*', '!node_modules/**/*', '!vendor', '!vendor/*', '!vendor/**/*', '!*.md', '!package.json', '!sublime-gulp.log', '!_*', '!**/_*']
  gulp.src(files, {dot: true})
    .pipe(gulp.dest('../dist'))
});

// watch for file changes
gulp.task('watch', function() {
	gulp.watch(['source/app/**/*.js', 'source/app/**/**/*.js', 'source/index.html', 'source/app/**/*.html', 'source/styles/**/*.scss', 'source/styles/*.scss', 'source/images/*.png', 'source/images/*.jpg'], [
		'app',
		'styles',
		'views',
		'images'
	]);
});
