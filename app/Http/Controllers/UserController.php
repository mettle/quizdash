<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use Config;
use Hash;
use JWTAuth;
use Response;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Ctct\ConstantContact;
use Ctct\Components\Contacts\Contact;
use Ctct\Exceptions\CtctException;

class UserController extends Controller {

  public function index() {
    if ($this->authenticatedUser->admin) {
      return $this->response(['users' => User::all()], true);
    } else {
      return $this->response(['errors' => ['Requires administrator privileges']], false, 403);
    }
  }

  public function store(Request $request) {
    if ($this->authenticatedUser->admin) {
      $validator = Validator::make($request->all(), [
        'username'    => 'required|unique:users|max:30',
        'email'       => 'required|unique:users|email|max:250',
        'source'      => 'max:250',
        'accesstoken' => 'max:100',
        'password'    => 'required|max:60'
      ]);

      if ($validator->fails()) {
        return $this->response(['errors' => $validator->errors()], false);
      }

      // Save email to Constant Contact
      $cc_key = Config::get('app.constantcontact_api_key');
      $cc_access_token = Config::get('app.constantcontact_access_token');
      $cc = new ConstantContact($cc_key);


      $response = $cc->contactService->getContacts($cc_access_token, array("email" => $request->get('email')));
      // create a new contact if one does not exist
      if (empty($response->results)) {
          $contact = new Contact();
          $contact->addEmail($request->get('email'));
          $contact->addList('daily');
//          $returnContact = $cc->contactService->addContact($cc_access_token, $contact);
      }

      $user = new User;

      $user->username     = $request->get('username');
      $user->email        = $request->get('email');
      if ($request->has('source'))      $user->source       = $request->get('source');
      if ($request->has('accesstoken')) $user->accesstoken  = $request->get('accesstoken');
      $user->password     = Hash::make($request->get('password'));
      $user->ip           = $request->ip;

      $user->save();

      return $this->response(['user' => $user], true);
    } else {
      return $this->response(['errors' => ['Requires administrator privileges']], false, 403);
    }
  }

  public function show($id) {
    if ($this->authenticatedUser->admin) {
      $user = User::findOrFail($id);

      // Calculate score values
      $user->score = $user->getScore();

      // We don't want to return the questions, so we'll unset them
      unset($user->questions);

      return $this->response(['user' => $user], true);
    } else {
      return $this->response(['errors' => ['Requires administrator privileges']], false, 403);
    }
  }

  public function update(Request $request, $id) {
    if ($this->authenticatedUser->admin) {
      $validator = Validator::make($request->all(), [
        'username'    => 'max:30',
        'email'       => 'email|max:250',
        'source'      => 'max:250',
        'accesstoken' => 'max:100',
        'password'    => 'max:60'
      ]);

      if ($validator->fails()) {
        return $this->response(['errors' => $validator->errors()], false);
      }

      $user = User::find($id);

      if ($request->has('username'))    $user->username     = $request->input('username');
      if ($request->has('email'))       $user->email        = $request->input('email');
      if ($request->has('source'))      $user->source       = $request->input('source');
      if ($request->has('accesstoken')) $user->accesstoken  = $request->input('accesstoken');
      if ($request->has('subscribe'))   $user->subscribe    = $request->input('subscribe');
      if ($request->has('admin'))       $user->admin        = $request->input('admin');
      if ($request->has('password'))    $user->password     = Hash::make($request->input('password'));

      $user->save();

      $cc_key = Config::get('app.constantcontact_api_key');
      $cc_access_token = Config::get('app.constantcontact_access_token');
      $cc_list_id = Config::get('app.constantcontact_list_id');
      $cc = new ConstantContact($cc_key);

      $response = $cc->contactService->getContacts($cc_access_token, array("email" => $user->email));
      if (!empty($response->results)) {
          $contact = $response->results[0];
          if ($contact instanceof Contact) {
            if ($request->input('subscribe')) {
              $contact->addList(strval($cc_list_id));
            } else {
              $contact->lists = [];
            }

            $returnContact = $cc->contactService->updateContact($cc_access_token, $contact);
          }
      } else {
        // User not found.
        if ($user->subscribe) {
          $contact = new Contact();
          $contact->addEmail($user->email);
          $contact->addList(strval($cc_list_id));

          $returnContact = $cc->contactService->addContact($cc_access_token, $contact);
        }
      }

      return $this->response(['user' => $user], true);
    } else {
      return $this->response(['errors' => ['Requires administrator privileges']], false, 403);
    }
  }

  public function destroy($id) {
    if ($this->authenticatedUser->admin) {
      User::destroy($id);

      return $this->response(null, true);
    } else {
      return $this->response(['errors' => ['Requires administrator privileges']], false, 403);
    }
  }
}
