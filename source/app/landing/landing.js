(function() {
    'use strict';

    angular
        .module('app.landing')
        .controller('Landing', Landing);

    Landing.$inject = ['$scope', '$state', 'Answers', 'Questions', 'Users', 'UserStore'];

    function Landing($scope, $state, Answers, Questions, Users, UserStore) {

        var vm = this;
        vm.user = UserStore.getUser();
        vm.submitAnswer = submitAnswer;
        vm.answered = false;

        $scope.$on('timer-stopped', function (event, data) {
            // Out of time-register an incorrect answer
            var params = {
              u_id: vm.user.id,
              q_id: vm.question.id,
              a_id: 0
            };
            if (!vm.answered) Users.submitAnswer(params, params).$promise.then(redirectToAnswer);
        });

        Questions.getCurrent().$promise.then(load);

        function load(question) {
            if (question.success) {
                vm.question = question.question;
                vm.question.posted = new Date(vm.question.posted.replace(/-/g,"/"));
                if (vm.question.scheduled) {
                    vm.question.display_date = new Date(vm.question.scheduled.replace(/-/g,"/"));
                } else {
                    vm.question.display_date = vm.question.posted;
                }

                // Now that the question is loaded, check if the user has answered it already
                if (vm.user.id) {
                  $scope.$broadcast('timer-start');
                  Users.getAnswer({
                    u_id: vm.user.id,
                    q_id: question.question.id
                  }).$promise.then(checkIfAnswered);
                }
            } else if (question.errors.length>0 && question.errors[0]=='No more questions queued') {
                vm.error = "Oops! Looks like we ran out of questions. If you have a doozie you'd like to share, <a href=\"/answer/submit\">we would love to hear it!</a>";
            } else {
                vm.error = question.errors;
            }
        }

        function submitAnswer() {
          if (!vm.answer) {
            // User hasn't selected an answer
          } else if (!vm.user.id) {
            // User isn't logged in-bounce them to login form
            $state.go('anon.login');
          } else {
            // Register the vote
            var params = {
              u_id: vm.user.id,
              q_id: vm.question.id,
              a_id: vm.answer
            };
            Users.submitAnswer(params, params).$promise.then(redirectToAnswer);
          }
        }

        function checkIfAnswered(response) {
          // If user has already answered, bounce them to the results page
          if (response.success) {
            vm.answered = true;
            $scope.$broadcast('timer-stop');
            $state.go('user.answer');
          }
        }

        function redirectToAnswer(response) {
          if (response.success) {
            // Update the UserStore with increment in correct/incorrect value
            vm.user.scoreAnswered++;
            if (response.user.questions[0].pivot.answer_id==response.user.questions[0].correct_answer.id) {
              // User answered correctly
              vm.user.scoreCorrect++;
            }
            console.dir(vm.user);
            UserStore.remember(vm.user);

            // Bounce them to the answer page
            $state.go('user.answer');
          } else {
            // API returned an error
          }
        }
    }
})();