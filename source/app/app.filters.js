	(function() {
	'use strict';

	angular
    .module('app')
    .filter("truncate", function() {
        return function(x) {
            if (x===null) {
              x = '';
            } else {
              x = x.substr(0, 20)+'...';
            }
            return x;
        };
    }).filter('renderHTML', function($sce) {
        return function(val) {
            return $sce.trustAsHtml(val);
        };
    });
 })();