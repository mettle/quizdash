(function() {
    'use strict';

    angular
        .module('app.refer')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'anon.refer',
                config: {
                    url: '/refer',
                    views: {
                        'content@': {
                            templateUrl: '/views/refer/refer.html',
                            controller: 'Refer',
                            controllerAs: 'vm'
                        },
                        'header@': {
                            templateUrl: '/views/user/header.html',
                            controller: 'Header',
                            controllerAs: 'vm'
                        },
                    }
                }
            }
        ];
    }
})();