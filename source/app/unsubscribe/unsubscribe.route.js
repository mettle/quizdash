(function() {
    'use strict';

    angular
        .module('app.unsubscribe')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'user.unsubscribe',
                config: {
                    url: '/unsubscribe',
                    views: {
                        'content@': {
                            templateUrl: '/views/unsubscribe/unsubscribe.html',
                            controller: 'Unsubscribe',
                            controllerAs: 'vm'
                        },
                        'header@': {
                            templateUrl: '/views/user/header.html',
                            controller: 'Header',
                            controllerAs: 'vm'
                        },
                    }
                }
            }
        ];
    }
})();