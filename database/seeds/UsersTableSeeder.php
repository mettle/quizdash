<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder {
  public function run() {
    DB::table('users')->delete();

    User::create(array(
      'username'    => 'asoell',
      'email'       => 'andy@mettleup.com',
      'accesstoken' => 'xxx',
      'password'    => Hash::make('supersecret'),
      'admin'       => true
    ));

    User::create(array(
      'username'    => 'rgarand',
      'email'       => 'rory@mettleup.com',
      'source'      => 'nyt',
      'accesstoken' => 'xxx',
      'password'    => Hash::make('supersecret')
    ));

    User::create(array(
      'username'    => 'zpritchett',
      'email'       => 'zoe@bighugemedia.com',
      'accesstoken' => 'xxx',
      'password'    => Hash::make('supersecret'),
      'admin'       => true
    ));
  }
}
