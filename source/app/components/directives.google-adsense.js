(function() {
    'use strict';

    angular
        .module('app.directives')
        .directive('googleAdsense', GoogleAdsense);

//    GoogleAdsense.$inject = [$scope];

    function GoogleAdsense() {
        var directive = {
        restrict: 'A',
        replace: true,
        template: function(elem, attr) {
          var adSlot = (attr.slot || 2916362538);
          var adClient = (attr.client || 4920618911503989);
          return '<ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-'+adClient+'" data-ad-slot="'+adSlot+'" data-ad-format="auto"></ins>';
        },
        controller: function () {
            var adsbygoogle = adsbygoogle || {};
            (adsbygoogle = window.adsbygoogle || []).push({});
        }
        };
        return directive;
    }
})();

