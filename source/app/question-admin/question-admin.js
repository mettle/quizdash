(function() {
    'use strict';

    angular
        .module('app.question-admin')
        .controller('QuestionAdmin', QuestionAdmin);

    QuestionAdmin.$inject = ['$scope', '$state', 'Answers', 'Questions', 'toastr', 'Upload', '$timeout'];

    function QuestionAdmin($scope, $state, Answers, Questions, toastr, Upload, $timeout) {

        var vm = this;

        vm.beginEditing = beginEditing;
        vm.collapse = collapse;
        vm.deleteRow = deleteRow;
        vm.doAddRecord = doAddRecord;
        vm.displayAddRecord = displayAddRecord;
        vm.editingCell = editingCell;
        vm.expand = expand;
        vm.finishEditing = finishEditing;
        vm.finishEditingAnswer = finishEditingAnswer;
        vm.resetAddRecord = resetAddRecord;
        vm.toggleCorrect = toggleCorrect;
        vm.uploadFiles = uploadFiles;

        vm.questions = [];

        Questions.get().$promise.then(load).catch(error);

        var editedCol, editedRow;

        function displayAddRecord() {
          vm.newRecord = {
            answers: [{},{},{},{}]
          };
        }

        function resetAddRecord() {
          vm.newRecord = null;
        }

        function doAddRecord() {
          Questions.save({}, vm.newRecord).$promise.then(finalizeAddRecord);
        }

        function finalizeAddRecord(response) {
          if (!response.success) {
            var detail = '';
            for (var field in response.errors) detail += response.errors[field][0]+' ';
            toastr.error("Unable to save record: "+detail, 'Error');
          } else {
            vm.questions.push(response.question);
            vm.newRecord = null;
            toastr.success('Saved!', 'Success');
          }
        }

        function beginEditing(row, col) {
            if (!editedRow && !editedCol) {
                editedRow = row.id;
                editedCol = col;

                vm.editedOriginalValue = row[col];
            }
        }

        function collapse(question) {
            delete question.expand;
            delete question.answers;
        }

        function deleteRow(question) {
          if (confirm('Are you sure you want to delete this item? This cannot be undone.')) {
            editedRow = question.id;
            Questions.delete({q_id: question.id}, question).$promise.then(finalizeDelete);
          }
        }

        function editingCell(row, col) {
            return (row.id === editedRow && col === editedCol) ? true : false;
        }

        function error(data) {
            console.log('error!');
            console.dir(data);
            //$state.go('anon.landing');
        }

        function expand(question) {
            Answers.get({q_id: question.id}).$promise.then(function(data) {
                console.dir(data);
                question.answers = data.question.answers;
                question.expand = true;
            });
        }

        function finishEditing(question) {
            if (editedRow && editedCol) {
                Questions.update({q_id: question.id}, question).$promise.then(finalizeEdit).finally(nullify);
            }
        }

        function finishEditingAnswer(question, answer) {
            if (editedRow && editedCol) {
                Answers.update({q_id: question.id, a_id: answer.id}, answer).$promise.then(finalizeAnswerEdit).finally(nullify);
            }
        }

        function finalizeAnswerEdit(response) {
            if (!response.success) {
                toastr.error('Edits to that Answer were not saved.', 'Error');
            } else {
                toastr.success('Saved!', 'Success');
            }
        }

        function finalizeEdit(response) {
            if (!response.success) {
                toastr.error('Could not save record.', 'Error');
                vm.questions[editedRow-1][editedCol] = vm.editedOriginalValue;
            } else {
                toastr.success('Saved!', 'Success');
            }
        }

        function finalizeDelete(response) {
          if (!response.success) {
            toastr.error('Could not delete record.', 'Error');
          } else {
            // Find and remove the item from the model
            for (var i=0; i<vm.questions.length; i++) {
              if (vm.questions[i].id == editedRow) {
                vm.questions.splice(i, 1);
                break;
              }
            }

            editedRow = null;
            toastr.success('Deleted!', 'Success');
          }
        }

        function load(question) {
            vm.questions = question.questions;
        }

        function nullify() {
            vm.editedOriginalValue = null;
            editedRow = null;
            editedCol = null;
        }

        function toggleCorrect(question, answer, index) {
            var i = question.answers.length;
            while (i--) {
                question.answers[i].correct = (index !== i) ? false : true;
            }
            Answers.update({q_id: question.id, a_id: answer.id}, answer).$promise.then(finalizeAnswerEdit);
        }

        function uploadFiles(file) {
          $scope.f = file;
          if (file && !file.$error) {
              file.upload = Upload.upload({
                  url: '/upload',
                  file: file
              });

              file.upload.then(function (response) {
                  vm.newRecord.image = response.data.filename;
                  $timeout(function () {
                      file.result = response.data;
                  });
              }, function (response) {
                  if (!response.success)
                      $scope.errorMsg = response.errors;
              });

              file.upload.progress(function (evt) {
                  file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
              });
          }
        }

    }
})();