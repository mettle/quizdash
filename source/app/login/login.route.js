(function() {
    'use strict';

    angular
        .module('app.login')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'anon.login',
                config: {
                    url: '/login',
                    views: {
                        'content@': {
                            templateUrl: '/views/login/login.html',
                            controller: 'Login',
                            controllerAs: 'vm'
                        },
                        'marketing': {
                            templateUrl: '/views/footer/marketing.html'
                        }
                        ,
                        'circles': {
                            templateUrl: '/views/footer/circles.html'
                        }
                    }
                }
            }
        ];
    }
})();