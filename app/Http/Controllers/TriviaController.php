<?php

namespace App\Http\Controllers;

use App\Trivia;
use Illuminate\Http\Request;
use Response;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TriviaController extends Controller {

  public function index() {
    return $this->response(['trivia' => Trivia::all()], true);
  }

  public function store(Request $request) {

    $validator = Validator::make($request->all(), [
      'title'      => 'required|string|max:255',
      'detail'     => 'required|string',
      'image'      => 'required|string|max:50',
      'scheduled'  => 'date|after:today'
    ]);

    if ($validator->fails()) {
      return $this->response(['errors' => $validator->errors()], false);
    }

    $trivia = new Trivia;

    $trivia->title      = $request->get('title');
    $trivia->detail     = $request->get('detail');
    $trivia->image      = $request->get('image');
    $trivia->scheduled  = $request->get('scheduled');

    $trivia->save();

    return $this->response(['trivia' => $trivia], true);
  }

  public function show($id) {
    return $this->response(['trivia' => Trivia::findOrFail($id)], true);
  }

  public function showToday() {
    // Check to see if an unshown question has already been assigned (posted) today
    $trivia = Trivia::postedToday()->get()->toArray();
    if (count($trivia)<3) {
      // First time today's trivia has been requested—see if one is scheduled
      $trivia = array_merge($trivia, Trivia::scheduledToday()->get()->toArray());

      // If not, grab the first question that hasn't been posted
      for ($i=count($trivia);$i<3;$i++) {
        $trivia = array_merge($trivia, Trivia::unposted()->get()->toArray());
      }
    }

    $trivia = array_slice($trivia, 0, 3);

    // TODO: Mark trivia as posted
    foreach ($trivia as $t) {
      $item = Trivia::find($t['id']);
      $item->posted = date("Y-m-d");
      $item->save();
    }

    return $this->response(['trivia' => $trivia], true);
  }


  public function update(Request $request, $id) {

    $validator = Validator::make($request->all(), [
      'title'      => 'string|max:255',
      'detail'     => 'string',
      'scheduled'  => 'date|after:today'
    ]);

    if ($validator->fails()) {
      return $this->response(['errors' => $validator->errors()], false);
    }

    $trivia = Trivia::find($id);

    $trivia->title      = $request->get('title');
    $trivia->detail     = $request->get('detail');
    $trivia->scheduled  = $request->get('scheduled');

    $trivia->save();

    return $this->response(['trivia' => $trivia], true);
  }

  public function destroy($id) {
    Trivia::destroy($id);

    return $this->response(null, true);
  }
}
