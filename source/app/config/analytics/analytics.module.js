(function() {
    'use strict';

    angular.module('config.analytics', [
        'angular-google-analytics'
    ]);
})();