(function() {
    'use strict';

    angular
        .module('app.unsubscribe')
        .controller('Unsubscribe', Unsubscribe);

    Unsubscribe.$inject = ['Users', 'UserStore'];

    function Unsubscribe(Users, UserStore) {
        var vm          = this;
        vm.user         = UserStore.getUser();
        vm.unsubscribe  = unsubscribe;
        vm.subscribe    = subscribe;

        function unsubscribe() {
          vm.user.subscribe = false;
          Users.update({u_id: vm.user.id }, vm.user);
          UserStore.remember(vm.user);

          return false;
        }

        function subscribe() {
          vm.user.subscribe = true;
          Users.update({u_id: vm.user.id }, vm.user);
          UserStore.remember(vm.user);

          return false;
        }
    }
})();