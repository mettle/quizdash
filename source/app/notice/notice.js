(function() {
    'use strict';

    angular
        .module('app.notice')
        .controller('Notice', Notice);

    Notice.$inject = ['UserStore', '$cookies'];

    function Notice(UserStore, $cookies) {

        var vm = this;
        vm.agree = agree;
        vm.user = UserStore.getUser();
        vm.agreed = $cookies.get('agreed');

        function agree() {
            var now = new Date();
            var exp = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());

            $cookies.put('agreed', now, { expires: exp });
            vm.agreed = now;
        }

    }
})();