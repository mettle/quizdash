<html>
    <body>
      <table border="0" width="100%">
        <tr><td bgcolor="#424242"><img src="http://quizdash.co.uk/images/logo-static.png" alt="Quiz Dash" title="Quiz Dash" /></td></tr>
        <tr><td>
          <h4><?php echo $question['question']; ?></h4>
          <p>How much do you know about UK trivia? A friend of yours has challenged you to join them on Quiz Dash, the exciting daily UK trivia site. Best all all: it's completely free. Sign up today and see if you know more than your friends: <a href="http://quizdash.co.uk">QuizDash.co.uk</a></p>
        </td></tr>
      </table>
      <small>This email was sent to <?php echo $email; ?> by a friend. You will not receive any more emails unless you specifically sign up for the Quiz Dash service.</small>
    </body>
</html>