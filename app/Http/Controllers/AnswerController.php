<?php

namespace App\Http\Controllers;

use App\Question;
use App\Answer;
use Illuminate\Http\Request;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AnswerController extends Controller {

  public function index($question_id) {
    return $this->response(['question' => Question::with(['answers','correctAnswer'])->find($question_id)], true);
  }

  public function store(Request $request, $question_id) {

    $validator = Validator::make($request->all(), [
      'question_id' => 'required|integer|exists:questions,id',
      'answer'      => 'required|string|max:255',
      'correct'     => 'required|boolean'
    ]);

    if ($validator->fails()) {
      return $this->response(['errors' => $validator->errors()], false);
    }

    $answer = new Answer;
    $answer->answer         = $request->get('answer');
    $answer->correct        = $request->get('correct');
    $answer->question_id    = $question_id;

    $answer->save();

    return $this->response(['answer' => $answer], true);
  }

  public function show($question_id, $answer_id) {
    return $this->response(['answer' => Answer::find($answer_id)], true);
  }

  public function update(Request $request, $question_id, $answer_id) {
    $validator = Validator::make($request->all(), [
      'answer'      => 'string|max:255',
      'correct'     => 'boolean'
    ]);

    if ($validator->fails()) {
      return $this->response(['errors' => $validator->errors()], false);
    }

    $answer = Answer::find($answer_id);

    // This should work but doesn't seem to — Set `correct` to false for all sibling answers if the answer we're updating is correct
    if ($request->has('correct') && $request->get('correct')) {
      Answer::where('question_id', $answer->question_id)->where('id','!=', $answer->id)->update(['correct' => false]);
    }

    if ($request->has('answer'))  $answer->answer     = $request->get('answer');
    if ($request->has('correct')) $answer->correct    = $request->get('correct');

    $answer->save();

    return $this->response(['answer' => $answer], true);
  }

  public function destroy($question_id, $answer_id) {
    Answer::destroy($answer_id);

    return $this->response(null, true);
  }
}
