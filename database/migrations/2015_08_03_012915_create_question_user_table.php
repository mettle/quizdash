<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionUserTable extends Migration {
  public function up() {
    Schema::create('question_user', function (Blueprint $table) {
      $table->integer('user_id')->references('id')->on('users');
      $table->integer('question_id')->references('id')->on('questions');
      $table->integer('answer_id')->references('id')->on('answers');
      $table->primary(['user_id', 'question_id']);
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('question_user');
  }
}
