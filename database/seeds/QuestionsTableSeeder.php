<?php

use App\Question;
use App\Answer;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class QuestionsTableSeeder extends Seeder {
  public function run() {
    DB::table('questions')->delete();
    DB::table('answers')->delete();

    $question = new Question;
    $question->question = 'Who was awarded the very first Gold Record?';
    $question->detail = 'The American recording industry gave out its first Gold Record award on March 14, 1958, to the Perry Como hit “Catch a Falling Star.” The single won Como the 1959 Grammy Award for Best Vocal Performance, Male. Introduced by the Recording Industry Association of America as a way “to create a standard by which to measure sales,” the Gold Award honored records that sold 500,000 copies in the US. Como has the distinction of having three stars on the Hollywood Walk of Fame for his work in radio, television, and music.';
    $question->save();

      $answer = new Answer;
      $answer->question_id  = $question->id;
      $answer->answer       = 'Elvis Presley';
      $answer->correct      = false;
      $answer->save();

      $answer = new Answer;
      $answer->question_id  = $question->id;
      $answer->answer       = 'Nat King Cole';
      $answer->correct      = false;
      $answer->save();

      $answer = new Answer;
      $answer->question_id  = $question->id;
      $answer->answer       = 'The Beatles';
      $answer->correct      = false;
      $answer->save();

      $answer = new Answer;
      $answer->question_id  = $question->id;
      $answer->answer       = 'Perry Como';
      $answer->correct      = true;
      $answer->save();

    $question = new Question;
    $question->question = 'What is the longest road in the United States?';
    $question->detail = 'According to the U.S. Department of Transportation website, the longest road in the United States is U.S. Route 20. US Route 20, which stretches 3,365 miles in an east-west direction from Boston, Massachusetts, to Newport, Oregon, is the seventh longest highway in the world and the longest road in the US. The highway\'s eastern terminus is in Boston, Massachusetts, at Kenmore Square, where it meets State Route 2. Its western terminus is in Newport, Oregon, at an intersection with US 101, within a mile of the Pacific Ocean. The highway was commissioned in 1926 and was extended in 1940.';
    $question->save();

      $answer = new Answer;
      $answer->question_id  = $question->id;
      $answer->answer       = 'I-95';
      $answer->correct      = false;
      $answer->save();

      $answer = new Answer;
      $answer->question_id  = $question->id;
      $answer->answer       = 'U.S. Route 20';
      $answer->correct      = true;
      $answer->save();

      $answer = new Answer;
      $answer->question_id  = $question->id;
      $answer->answer       = 'Route 66';
      $answer->correct      = false;
      $answer->save();

      $answer = new Answer;
      $answer->question_id  = $question->id;
      $answer->answer       = 'U.S. Route 1';
      $answer->correct      = false;
      $answer->save();

    $question = new Question;
    $question->question = 'Which of these landmarks is in India?';
    $question->detail = 'The Taj Mahal is one of the world’s most celebrated structures and a symbol of India’s rich history. The Taj Mahal is a white marble mausoleum located on the southern bank of Yamuna River in the Indian city of Agra. It was commissioned in 1632 by the Mughal emperor Shah Jahan (reigned 1628–1658) to house the tomb of his favorite wife of three, Mumtaz Mahal. The Taj Mahal is regarded by many as the best example of Mughal architecture and is widely recognized as "the jewel of Muslim art in India". The Taj Mahal attracts some 3 million visitors a year.';
    $question->save();

      $answer = new Answer;
      $answer->question_id  = $question->id;
      $answer->answer       = 'Pyramids of Giza';
      $answer->correct      = false;
      $answer->save();

      $answer = new Answer;
      $answer->question_id  = $question->id;
      $answer->answer       = 'Machu Picchu';
      $answer->correct      = false;
      $answer->save();

      $answer = new Answer;
      $answer->question_id  = $question->id;
      $answer->answer       = 'The Taj Mahal';
      $answer->correct      = true;
      $answer->save();

      $answer = new Answer;
      $answer->question_id  = $question->id;
      $answer->answer       = 'Palace of Versailles';
      $answer->correct      = false;
      $answer->save();
  }
}
