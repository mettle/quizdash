<?php

namespace App\Http\Controllers;

use JWTAuth;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UploadController extends Controller {

  public function upload(Request $request) {
    // Code for uploading an image
    if (!$request->hasFile('file'))
      return $this->response(['errors' => ['No file provided']], false);
    if (!$request->file('file')->isValid())
      return $this->response(['errors' => ['Could not complete file upload']], false);
    if ($request->file('file')->getMimeType() != 'image/jpeg' &&
        $request->file('file')->getMimeType() != 'image/png' &&
        $request->file('file')->getMimeType() != 'image/gif')
      return $this->response(['errors' => ['Please upload a JPG, PNG, or GIF formatted image']], false);

    $fileName = (microtime(true)*10000).'.'.$request->file('file')->guessExtension();
    $request->file('file')->move(public_path('assets'), $fileName);

    return $this->response(['filename' => $fileName], true);
  }
}
