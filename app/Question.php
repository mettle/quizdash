<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Question extends Model {
  public function answers() {
    return $this->hasMany('App\Answer');
  }

  public function correctAnswer() {
    return $this->hasOne('App\Answer')->where('correct', 1);
  }

  public function users() {
    return $this->belongsToMany('App\User')->withTimestamps()->withPivot('answer_id');
  }

  public function scopePostedToday($query) {
    $query->with(['answers'])->where('posted', date("Y-m-d"))->limit(1);
  }

  public function scopeScheduledToday($query) {
    $query->with(['answers'])->where('scheduled', date("Y-m-d"))->limit(1);
  }

  public function scopeUnposted($query) {
    $query->with(['answers'])->whereNull('posted');
  }

  protected $dates = ['scheduled','posted'];
}
