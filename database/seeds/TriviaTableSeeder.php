<?php

use App\Trivia;
use Illuminate\Database\Seeder;

class TriviaTableSeeder extends Seeder {
  public function run() {
    DB::table('trivia')->delete();

    Trivia::create(array(
      'title'       => 'An 11-year-old girl gave Pluto its name.'
    ));

    Trivia::create(array(
      'title'       => 'Today in history',
      'detail'      => 'August 4, 1914: Britain declared war on Germany while the United States proclaimed its neutrality.',
      'scheduled'   => '2015-08-04'
    ));

    Trivia::create(array(
      'title'       => 'Today\'s Birthdays',
      'detail'      => '1961 - Barack Obama, 1955 - Billy Bob Thornton, (1821-1892) - Louis Vuitton',
      'scheduled'   => '2015-08-04'
    ));
  }
}
