(function() {
    'use strict';

    angular
        .module('app.directives')
        .directive('clickFocus', ClickFocus);

    ClickFocus.$inject = ['$timeout'];

    function ClickFocus($timeout) {
    	var directive = {
    		link: link
    	};
    	return directive;

    	//link.$inject = ['scope', 'element', 'attrs'];

    	function link(scope, element, attrs) {
    		scope.$watch(attrs.clickFocus, changeTo);

    		function changeTo(newVal) {
    			if (newVal) {
    				$timeout(function(){element[0].focus();}, 0, false);
    			}
    		}
    	}
    }
})();