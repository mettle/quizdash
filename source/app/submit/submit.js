(function() {
    'use strict';

    angular
        .module('app.submit')
        .controller('Submit', Submit);

    Submit.$inject = ['Contact'];

    function Submit(Contact) {

        var vm = this;
        vm.submitQuestion = submitQuestion;
        vm.resetForm = resetForm;

        vm.question = {};

        vm.submitQuestion = submitQuestion

        function submitQuestion() {
          Contact.sendQuestion({}, vm.question);
          vm.emailSent=true;

          return false;
        }

        function resetForm() {
          vm.question = {};
          vm.emailSent = false;
        }
    }
})();