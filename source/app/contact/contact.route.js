(function() {
    'use strict';

    angular
        .module('app.contact')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'anon.contact',
                config: {
                    url: '/contact',
                    views: {
                        'content@': {
                            templateUrl: '/views/contact/contact.html',
                            controller: 'Contact',
                            controllerAs: 'vm'
                        },
                        'header@': {
                            templateUrl: '/views/user/header.html',
                            controller: 'Header',
                            controllerAs: 'vm'
                        },
                    }
                }
            }
        ];
    }
})();