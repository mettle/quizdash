<html>
    <body>
      <table border="0" width="100%">
        <tr><td bgcolor="#424242"><img src="http://quizdash.co.uk/images/logo-static.png" alt="Quiz Dash" title="Quiz Dash" /></td></tr>
        <tr><td>
          <h4>Someone is thinking of you...</h4>
          <p>And they want you to play the hot new quiz game sweeping the UK! Sign up today for free at <a href="http://quizdash.co.uk">QuizDash.co.uk</a></p>
        </td></tr>
      </table>
      <small>This email was sent to <?php echo $email; ?> by a friend. You will not receive any more emails unless you specifically sign up for the Quiz Dash service.</small>
    </body>
</html>