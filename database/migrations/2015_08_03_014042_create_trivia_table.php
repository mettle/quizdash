<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriviaTable extends Migration {
  public function up() {
    Schema::create('trivia', function (Blueprint $table) {
      $table->increments('id');
      $table->string('title', 255);
      $table->text('detail')->nullable();
      $table->text('image', 50)->nullable();
      $table->date('scheduled')->nullable();
      $table->date('posted')->nullable();
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('trivia');
  }
}
