(function() {
    'use strict';

    angular
        .module('app.landing')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'anon.landing',
                config: {
                    url: '/',
                    views: {
                        'content@': {
                            templateUrl: '/views/landing/landing.html',
                            controller: 'Landing',
                            controllerAs: 'vm'
                        },
                        'ad': {
                            templateUrl: '/views/footer/ad.html'
                        }
                    }
                }
            }
        ];
    }
})();