(function() {
    'use strict';

    angular
        .module('app.about')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'anon.about',
                config: {
                    url: '/about',
                    views: {
                        'content@': {
                            templateUrl: '/views/about/about.html',
                            controller: 'About',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
        ];
    }
})();