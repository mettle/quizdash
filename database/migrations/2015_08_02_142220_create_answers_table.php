<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration {
  public function up() {
    Schema::create('answers', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('question_id')->unsigned();
      $table->string('answer');
      $table->boolean('correct');
      $table->timestamps();
      $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
    });
  }

  public function down() {
    Schema::drop('answers');
  }
}
