(function() {
    'use strict';

    angular
        .module('app.faq')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'anon.faq',
                config: {
                    url: '/faq',
                    views: {
                        'content@': {
                            templateUrl: '/views/faq/faq.html',
                            controller: 'FAQ',
                            controllerAs: 'vm'
                        },
                        'header@': {
                            templateUrl: '/views/user/header.html',
                            controller: 'Header',
                            controllerAs: 'vm'
                        },
                    }
                }
            }
        ];
    }
})();