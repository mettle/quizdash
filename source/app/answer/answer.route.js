(function() {
    'use strict';

    angular
        .module('app.answer')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'user.answer',
                config: {
                    url: '',
                    views: {
                        'content@': {
                            templateUrl: '/views/answer/answer.html',
                            controller: 'Answer',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
        ];
    }
})();