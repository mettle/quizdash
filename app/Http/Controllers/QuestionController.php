<?php

namespace App\Http\Controllers;

use JWTAuth;
use Response;
use Validator;
use App\Question;
use App\Answer;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuestionController extends Controller {

  public function index() {
    if ($this->authenticatedUser && $this->authenticatedUser->admin) {
      return $this->response(['questions' => Question::all()], true);
    } else {
      return $this->response(['errors' => ['Requires administrator privileges']], false, 403);
    }
  }

  public function store(Request $request) {
    if ($this->authenticatedUser->admin) {
      $validator = Validator::make($request->all(), [
        'question'   => 'required|string|max:255',
        'detail'     => 'required|string',
        'image'      => 'string|max:50',
        'scheduled'  => 'date|after:today'
      ]);

      if ($validator->fails()) {
        return $this->response(['errors' => $validator->errors()], false);
      }

      $question = new Question;

      $question->question     = $request->get('question');
      $question->detail       = $request->get('detail');
      $question->image        = $request->get('image');
      if ($request->has('scheduled')) $question->scheduled = $request->get('scheduled');

      if ($question->save()) {
        // Now save the answers
        $answers = [];
        foreach ($request->get('answers') as $a) {
          $answer = new Answer;
          $answer->question_id    = $question->id;
          $answer->answer         = $a['answer'];
          if (array_key_exists('correct', $a)) $answer->correct = $a['correct'];

          if ($answer->save()) {
            $answers[] = $answer;
          }
        }

        $question->answers = $answers;
      }

      return $this->response(['question' => $question], true);
    } else {
      return $this->response(['errors' => ['Requires administrator privileges']], false, 403);
    }
  }

  public function show($id) {
    if ($this->authenticatedUser->admin) {
      return $this->response(['question' => Question::find($id)], true);
    } else {
      return $this->response(['errors' => ['Requires administrator privileges']], false, 403);
    }
  }

  public function showCurrent() {
      $question = Question::with('answers')->where('scheduled','<=', date('Y-m-d H:i:s'))->orderBy('scheduled', 'desc')->first();
      if ($question) {
          if (!$question->posted) {
              $question->posted = date("Y-m-d H:i:s");
              $question->save();
          }
          return $this->response(['question' => $question], true);
      } else {
          return $this->response(['errors' => ['No more questions queued']], false);
      }
  }

  public function update(Request $request, $id) {
    if ($this->authenticatedUser->admin) {
      $validator = Validator::make($request->all(), [
        'question'   => 'string|max:255',
        'detail'     => 'string',
        'scheduled'  => 'date'
      ]);

      if ($validator->fails()) {
        return $this->response(['errors' => $validator->errors()], false);
      }

      $question = Question::find($id);

      if ($request->has('question'))  $question->question     = $request->get('question');
      if ($request->has('detail'))    $question->detail       = $request->get('detail');
      if ($request->has('scheduled')) $question->scheduled    = $request->get('scheduled');

      $question->save();

      return $this->response(['success' => true, 'question' => $question], true);
    } else {
      return $this->response(['errors' => ['Requires administrator privileges']], false, 403);
    }
  }

  public function destroy($id) {
    if ($this->authenticatedUser->admin) {
      Question::destroy($id);

      return $this->response(null, true);
    } else {
      return $this->response(['errors' => ['Requires administrator privileges']],false, 403);
    }
  }
}
