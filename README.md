## QuizDaily-Laravel

Laravel backend application for the QuizDaily.co.uk site. Eventually we'll roll the [quizdaily-angular](/mettle/quizdaily-angular) repo into this one and just rename it to "quizdaily"
