<?php

namespace App\Http\Controllers;

use App\User;
use App\Question;
use App\Answer;
use Response;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserQuestionController extends Controller {

  /* Get list of questions the selected user has participated with */
  public function index($user_id) {
    return $this->response(['user' => User::with('questions')->find($user_id)], true);
  }

  /* Return a user's interaction with a specific question */
  public function show($user_id, $question_id) {
    $user = User::find($user_id);
    $user->load(['questions' => function ($q) use ($question_id) {
      $q->find($question_id);
    }]);

    if ($user->questions->find($question_id)) {
      return $this->response(['question' => Question::find($question_id), 'answer' => Answer::find($user->questions->find($question_id)->pivot['answer_id'])], true);
    } else {
      return $this->response(['errors' => array('User has not submitted an answer to this question')], false);
    }
  }
}
