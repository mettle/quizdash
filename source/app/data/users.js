(function() {
    'use strict';

    angular
        .module('app.data')
        .factory('Users', Users);

    Users.$inject = ['$resource'];

    function Users($resource) {
    	return $resource('/api/v1/users/:u_id/:q_url/:q_id/:a_url/:a_id', {u_id: this.u_id, q_id: this.q_id, a_id: this.a_id}, {
            submitAnswer: {method: 'PUT', params:{q_url: 'questions', a_url: 'answers'}},
            getAnswer: {method: 'GET', params:{q_url: 'questions'}},
            deleteAnswer: {method: 'DELETE', params: {q_url: 'questions', a_url: 'answers'}},
            update: {method: 'PUT'}
        });
    }
})();