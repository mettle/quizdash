(function() {
    'use strict';

    angular
        .module('app.header')
        .controller('Header', Header);

    Header.$inject = ['UserStore', '$auth', '$rootScope', '$state'];

    function Header(UserStore, $auth, $rootScope, $state) {

        var vm = this;

        vm.user = UserStore.getUser();
        vm.logout = logout;

        $rootScope.$on('user:remembered', updateUser);

        function logout() {
            UserStore.reset();
            $auth.logout();
            $state.go('anon.landing');
        }

        function updateUser(user) {
          vm.user= UserStore.getUser();
        }

    }
})();