(function() {
    'use strict';

    angular
        .module('app.directives')
        .directive('taboola', Taboola);

//    Taboola.$inject = [$scope];

    function Taboola() {
        var directive = {
            restrict: 'A',
            replace: true,
            template: function(elem, attr) {
              var mode = (attr.mode || 'thumbnails-a');
              var container = (attr.container || 'below-article-thumbnails');
              var placement = (attr.placement || 'Below Article Thumbnails');
              var target_type = (attr.target_type || 'mix');

              return '<div id="taboola-'+container+'"></div>';
            },
            controller: function () {
                window._taboola = window._taboola || [];
                _taboola.push({
                    mode: 'thumbnails-a',
                    container: 'taboola-below-article-thumbnails',
                    placement: 'Below Article Thumbnails',
                    target_type: 'mix'
                });

//                 window._taboola = window._taboola || [];
                _taboola.push({flush: true});
            }
        };
        return directive;
    }
})();

