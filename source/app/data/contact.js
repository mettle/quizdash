(function() {
    'use strict';

    angular
        .module('app.data')
        .factory('Contact', Contact);

    Contact.$inject = ['$resource'];

    function Contact($resource) {
    	return $resource('/contact/:action', {action: this.action}, {
            sendContact: {method: 'POST', params: {action: 'sendContact'}},
            sendQuestion: {method: 'POST', params: {action: 'sendQuestion'}},
            sendRefer: {method: 'POST', params: {action: 'sendRefer'}},
            shareQuestion: {method: 'POST', params: {action: 'shareQuestion'}}
        });
    }
})();