(function() {
    'use strict';

    angular
        .module('app.store')
        .factory('UserStore', UserStore);

    UserStore.$inject = ['$rootScope', '$cookies', 'md5'];

    function UserStore($rootScope, $cookies, md5) {
        this.getUser = getUser;
        this.remember = remember;
        this.reset = reset;

        return this;

        function getUser() {
            var user = {
                email: $cookies.get('email'),
                avatar: $cookies.get('avatar'),//http://www.gravatar.com/avatar/'+(md5.createHash($cookies.get('email') || ''))+'/?s=50',
                id: $cookies.get('id'),
                username: $cookies.get('username'),
                admin: ($cookies.get('admin')==1||$cookies.get('admin')=="true"||$cookies.get('admin')===true)?true:false,
                subscribe: ($cookies.get('subscribe')==1||$cookies.get('subscribe')==="true"||$cookies.get('subscribe')===true)?true:false,
                scoreAnswered: $cookies.get('score-answered'),
                scoreCorrect: $cookies.get('score-correct')
            };

            return user;
        }

        function remember(user) {
            var now = new Date();
            var exp = new Date(now.getFullYear()+1, now.getMonth(), now.getDate());


            $cookies.put('email', user.email, { expires: exp });
            $cookies.put('avatar', user.avatar, { expires: exp });
            $cookies.put('id', user.id, { expires: exp });
            $cookies.put('username', user.username, { expires: exp });
            $cookies.put('admin', user.admin, { expires: exp });
            $cookies.put('subscribe', user.subscribe, { expires: exp });
            if (user.score) {
              if (user.score.answered) $cookies.put('score-answered', user.score.answered, { expires: exp });
              if (user.score.correct)  $cookies.put('score-correct', user.score.correct, { expires: exp });
            }
            if (user.scoreAnswered) {
              $cookies.put('score-answered', user.scoreAnswered, { expires: exp });
            }
            if (user.scoreCorrect) {
              $cookies.put('score-correct', user.scoreCorrect, { expires: exp });
            }

            $rootScope.$broadcast('user:remembered', user);
        }

        function reset() {
            $cookies.remove('email');
            $cookies.remove('avatar');
            $cookies.remove('id');
            $cookies.remove('username');
            $cookies.remove('subscribe');
            $cookies.remove('admin');
            $cookies.remove('score-answered');
            $cookies.remove('score-correct');
        }
    }
})();