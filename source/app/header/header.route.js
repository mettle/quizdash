(function() {
    'use strict';

    angular
        .module('app.header')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'anon.header',
                abstract: true,
                config: {
                    url: '',
                    views: {}
                }
            }
        ];
    }
})();