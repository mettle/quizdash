	(function() {
	'use strict';

	angular
		.module('app', [
			// Application
			'app.about',
			'app.admin',
			'app.anon',
			'app.answer',
			'app.contact',
			'app.faq',
			'app.header',
			'app.landing',
			'app.login',
			'app.privacy',
			'app.profile',
			'app.question-admin',
			'app.refer',
			'app.submit',
			'app.terms',
			'app.unsubscribe',
			'app.user',
			'app.user-admin',
			'app.notice',

			// Components
			'app.directives',

			// Services
			'app.data',
			'app.store',

			// Configuration
			'config.analytics',
			'config.bootstrap',
			'config.cookies',
			'config.md5',
			'config.router',
			'config.satellizer',
			'config.timer',
			'config.toastr',

			// Vendor
			'ngFileUpload',
			'angulike'
		]);
})();