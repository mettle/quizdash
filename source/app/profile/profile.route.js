(function() {
    'use strict';

    angular
        .module('app.profile')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'user.profile',
                config: {
                    url: '/profile',
                    views: {
                        'content@': {
                            templateUrl: '/views/profile/profile.html',
                            controller: 'Profile',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
        ];
    }
})();