(function() {
    'use strict';

    angular
        .module('app.user-admin')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'admin.users',
                config: {
                    url: '/admin/users',
                    views: {
                        'content@': {
                            templateUrl: '/views/user-admin/user-admin.html',
                            controller: 'UserAdmin',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
        ];
    }
})();