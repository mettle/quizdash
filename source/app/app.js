(function() {
    'use strict';

    angular
        .module('app')
        .run(Run);

    Run.$inject = ['$rootScope', '$auth', '$state', 'UserStore', 'Analytics'];

    function Run($rootScope, $auth, $state, UserStore, Analytics) {
        $rootScope.$on('$stateChangeError', stateChangeError);
        $rootScope.$on('$stateChangeStart', stateChangeStart);
        $rootScope.$on('$stateChangeSuccess', function() {
           document.body.scrollTop = document.documentElement.scrollTop = 0;
        });

        function stateChangeError(e, toState, toParams, fromState, fromParams) {
            e.preventDefault();
            $state.go('anon.landing', null, {notify: false});
        }

        function stateChangeStart(e, toState, toParams, fromState, fromParams) {
            var user = UserStore.getUser();
            if(toState.data.requireLogin && !$auth.isAuthenticated()) {
                e.preventDefault();
                UserStore.reset();
                $state.go('anon.login');
            } else if (toState.data.requireAdmin && !user.admin) {
              e.preventDefault();
              $state.go('anon.landing');
            }
        }
    }
})();