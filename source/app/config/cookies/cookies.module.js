(function() {
    'use strict';

    angular.module('config.cookies', [
        'ngCookies'
    ]);
})();