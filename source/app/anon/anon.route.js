(function() {
    'use strict';

    angular
        .module('app.anon')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'anon',
                config: {
                    url: '',
                    abstract: true,
                    views: {
                        'header@': {
                            templateUrl: '/views/anon/header.html',
                            controller: 'Header',
                            controllerAs: 'vm'
                        },
                        'footer@': {
                            templateUrl: '/views/footer/footer.html'
                        },
                        'notice@': {
                            templateUrl: '/views/notice/cookies.html',
                            controller: 'Notice',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        requireLogin: false
                    }
                }
            }
        ];
    }
})();