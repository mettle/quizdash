(function() {
    'use strict';

    angular
        .module('config.toastr')
        .config(Toastr);

    Toastr.$inject = ['toastrConfig'];

    function Toastr(toastrConfig) {
        var config = {
            positionClass: 'toast-bottom-right'
        };

        angular.extend(toastrConfig, config);
    }
})();