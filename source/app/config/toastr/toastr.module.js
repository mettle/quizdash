(function() {
    'use strict';

    angular.module('config.toastr', [
        'toastr'
    ]);
})();