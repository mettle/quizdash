(function() {
    'use strict';

    angular
        .module('config.analytics')
        .config(Analytics);

    Analytics.$inject = ['AnalyticsProvider'];

    function Analytics(AnalyticsProvider) {
        AnalyticsProvider
            .setAccount('UA-43400203-1')
            .setPageEvent('$stateChangeSuccess')
            .setDomainName('quizdash.co.uk');

    }
})();