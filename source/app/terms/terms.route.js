(function() {
    'use strict';

    angular
        .module('app.terms')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'anon.terms',
                config: {
                    url: '/terms',
                    views: {
                        'content@': {
                            templateUrl: '/views/terms/terms.html',
                            controller: 'Terms',
                            controllerAs: 'vm'
                        },
                        'circles': {
                            templateUrl: '/views/footer/circles.html'
                        }
                    }
                }
            }
        ];
    }
})();