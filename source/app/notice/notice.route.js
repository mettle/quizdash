(function() {
    'use strict';

    angular
        .module('app.notice')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'anon.notice',
                abstract: true,
                config: {
                    url: '',
                    views: {}
                }
            }
        ];
    }
})();