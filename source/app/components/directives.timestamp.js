(function() {
    'use strict';

    angular
        .module('app.directives')
        .directive('timestamp', Timestamp);

//    Timestamp.$inject = [$scope];

    function Timestamp() {
        var directive = {
            restrict: 'E',
            replace: true,
            link: function (scope, element, attr) {
                element.text(new Date().getFullYear());
            }

        };
        return directive;
    }
})();

