(function() {
    'use strict';

    angular
        .module('app.refer')
        .controller('Refer', Refer);

    Refer.$inject = ['Contact'];

    function Refer(Contact) {

        var vm = this;
        vm.recipients = {};

        vm.submitRefer = submitRefer

        function submitRefer() {
          Contact.sendRefer({}, vm.recipients);
          vm.emailSent=true;

          return false;
        }

    }
})();