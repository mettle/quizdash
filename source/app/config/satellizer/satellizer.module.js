(function() {
    'use strict';

    angular.module('config.satellizer', [
        'satellizer'
    ]);
})();