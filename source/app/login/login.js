(function() {
    'use strict';

    angular
        .module('app.login')
        .controller('Login', Login);

    Login.$inject = ['$auth', '$state', 'UserStore', 'toastr'];

    function Login($auth, $state, UserStore, toastr) {

        var vm = this;

        vm.authenticate = authenticate;
        vm.login = login;

        vm.user = UserStore.getUser();
        if (vm.user.id) {
            // User is already logged in- bounce to landing
            $state.go('anon.landing');
        }

        function authenticate(provider) {
            $auth.authenticate(provider).then(success).catch(error);
        }

        function error(data) {
            console.log('Error authenticating user.', 'Error');
            toastr.error('Unable to login. Please check your username and password');
            vm.email = "";
            vm.pw = "";
            console.dir(data);
        }

        function login() {
            $auth.login({email: vm.email, password: vm.pw}).then(success).catch(error);
        }

        function success(data) {
          console.log('success!');
          console.dir(data);
            UserStore.remember(data.data.user);
            $state.go('anon.landing');
        }
    }
})();