<?php

namespace App\Http\Controllers;

use Response;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;

    var $authenticatedUser;

    public function __construct(Request $request) {
      $this->authenticatedUser = $this->getAuthenticatedUser($request);

    }

    public function response($data, $success = true, $status = 200) {
      if (is_null($data)) $data = [];
      $data['authenticatedUser'] = $this->authenticatedUser;
      return Response::json(array_merge(['success' => $success], $data), $status);
    }

  public function getAuthenticatedUser(Request $request) {
    try {
      if (! $user = User::find($request['user']['sub'])) {
        return false;
      }
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
      return false;
    }
    return $user;
  }
}
