(function() {
    'use strict';

    angular
        .module('app.data')
        .factory('Questions', Questions);

    Questions.$inject = ['$resource'];

    function Questions($resource) {
        return $resource('/api/v1/questions/:q_id', {q_id: this.q_id}, {
            getCurrent: {method: 'GET', params: {q_id: 'current'}},
            update: {method: 'PUT'}
        });
    }
})();