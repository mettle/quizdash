<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
  public function up() {
    Schema::create('users', function (Blueprint $table) {
      $table->increments('id');
      $table->string('username')->unique()->index();
      $table->string('email')->index();
      $table->string('avatar', 500)->nullable();
      $table->string('facebook')->unique()->nullable();
      $table->string('twitter')->unique()->nullable();
      $table->boolean('admin')->default(0);
      $table->string('source')->default('direct');
      $table->boolean('subscribe')->default(1);
      $table->string('accesstoken', 100);
      $table->string('password', 60);
      $table->string('ip', 45)->nullable();
      $table->rememberToken();
      $table->timestamps();
      $table->softDeletes();
    });
  }

  public function down() {
    Schema::drop('users');
  }
}
