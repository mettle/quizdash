(function() {
    'use strict';

    angular
        .module('app.admin')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'admin',
                config: {
                    url: '',
                    abstract: true,
                    views: {
                        'header@': {
                            templateUrl: '/views/user/header.html',
                            controller: 'Header',
                            controllerAs: 'vm'
                        },
                        'footer@': {
                            templateUrl: '/views/footer/footer.html'
                        }

                    },
                    data: {
                        requireLogin: true,
                        requireAdmin: true
                    }
                }
            }
        ];
    }
})();