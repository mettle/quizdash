(function() {
    'use strict';

    angular
        .module('app.contact')
        .controller('Contact', Contact);

    Contact.$inject = ['Contact'];

    function Contact(Contact) {

        var vm = this;
        vm.message = {};

        vm.submitContact = submitContact

        function submitContact() {
          Contact.sendContact({}, vm.message);
          vm.emailSent=true;

          return false;
        }
    }
})();