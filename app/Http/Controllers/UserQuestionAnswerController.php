<?php

namespace App\Http\Controllers;

use App\User;
use App\Question;
use App\Answer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserQuestionAnswerController extends Controller {

  // Get the users' selection to the indicated question
  public function index($user_id, $question_id) {
    return $this->response(['errors' => ['not configured']], false);
  }

  // Set the user's selection to the indicated question
  public function update(Request $request, $user_id, $question_id, $answer_id) {
    $user     = User::find($user_id);
    $user->load(['questions' => function ($q) use ($question_id) {
      $q->find($question_id);
    }]);

    // Detatch any previously selected answer
    $user->questions()->detach($question_id);

    // Save the answer
    $user->questions()->attach($question_id, ["answer_id" => $answer_id]);

    // Reset it to only include the currently involved question
    $user     = User::find($user_id);
    $user->load(['questions' => function ($q) use ($question_id) {
      $q->find($question_id);
    }]);

    return $this->response(['user' => $user], true);
  }

  // Keeping the POST/store method intact just in case
  public function store(Request $request, $user_id, $question_id) {
    return $this->update($request, $user_id, $question_id, $request->answer_id);
  }


  // "Unanswer" the selected question
  public function destroy($user_id, $question_id, $answer_id) {
    $user = User::find($user_id);
    $user->load(['questions' => function ($q) use ($question_id) {
      $q->find($question_id);
    }]);

    // Detatch any previously selected answer
    $user->questions()->detach($question_id);

    return $this->response(null, true);
  }
}
