(function() {
    'use strict';

    angular
        .module('app.answer')
        .controller('Answer', Answer);

    Answer.$inject = ['$state', 'Questions', 'Users', 'UserStore', 'Contact', 'Blog', '$window', '$location'];

    function Answer($state, Questions, Users, UserStore, Contact, Blog, $window, $location) {

        var vm = this;
        vm.shareFacebook = shareFacebook;
        vm.shareTwitter = shareTwitter;
        vm.shareEmailInit = shareEmailInit;
        vm.shareEmailReset = shareEmailReset;
        vm.shareEmail = shareEmail;
        vm.sharePost = sharePost;
        vm.answered = false;
        vm.style = "white";

        Questions.getCurrent().$promise.then(loadQuestions);
        Blog.getPosts().$promise.then(loadBlogPosts);

        var user = UserStore.getUser();

        function loadQuestions(question) {
            vm.question = question.question;
            vm.question.posted = new Date(vm.question.posted.replace(/-/g,"/"));

          // Now that the question is loaded, check if the user has answered it already
          if (user.id) {
            Users.getAnswer({
              u_id: user.id,
              q_id: question.question.id
            }).$promise.then(checkIfAnswered);
          }
        }

        function loadBlogPosts(posts) {
            vm.posts = posts.posts.slice(0,3);
        }

        function checkIfAnswered(response) {
          if (!response.success) {
            $state.go('anon.landing');
          } else {
            vm.answered = true;
            vm.correct = response.answer.correct;
          }
        }

        function shareEmailInit() {
          vm.shareEmailDetail = {};
        }

        function shareEmailReset() {
          vm.shareEmailDetail = null;
        }

        function shareEmail() {
          vm.shareEmailDetail.question = vm.question;
          Contact.shareQuestion({}, vm.shareEmailDetail);
          vm.shareEmailDetail.success = true;
        }

        function shareFacebook(type, id) {
          $window.open('http://www.facebook.com/sharer.php?u=http://'+$location.host()+'/'+type+'/'+id+'&t=Quiz+Dash', 'facebookShare', 'height=400,width=800,menubar=no,status=no');
        }

        function shareTwitter(type, id) {
          $window.open('https://twitter.com/intent/tweet?text=Quiz%20Dash&url=http://'+$location.host()+'/'+type+'/'+id, 'twitterShare', 'height=400,width=800,menubar=no,status=no');
        }

        function sharePost(post) {
            $window.open('http://www.facebook.com/sharer.php?s=100&p[title]='+post.title+'&p[summary]='+post.summary+'&p[url]='+post.url+'&p[images][0]='+post.thumbnail_images.full.url, 'sharer', 'toolbar=0,status=0,width=548,height=325');
        }

    }
})();