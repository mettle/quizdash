<?php

namespace App;

use Carbon;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model {
  public function question() {
    $this->belongsTo('App\Question');
  }

  protected $casts = ['correct' => 'boolean'];
}
