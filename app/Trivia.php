<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class Trivia extends Model {

  public function scopePostedToday($query) {
    $query->where('posted', date("Y-m-d"));
  }

  public function scopeScheduledToday($query) {
    $query->where('scheduled', date("Y-m-d"));
  }

  public function scopeUnposted($query) {
    $query->whereNull('posted');
  }

  protected $table = 'trivia';
}
