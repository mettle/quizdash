<?php

Route::get('/', function () {
	return File::get(public_path() . '/angular.html');
});

Route::post('contact/sendContact', 'ContactController@sendContact');
Route::post('contact/sendRefer', ['middleware' => 'auth', 'uses' => 'ContactController@sendRefer']);
Route::post('contact/sendQuestion', ['middleware' => 'auth', 'uses' => 'ContactController@sendQuestion']);
Route::post('contact/shareQuestion', ['middleware' => 'auth', 'uses' => 'ContactController@shareQuestion']);
Route::get ('contact/unsubscribe', 'ContactController@unsubscribe');

Route::post('upload', 'UploadController@upload');

Route::group(array('prefix' => 'api/v1'), function() {

  Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
  Route::post('authenticate', 'AuthenticateController@authenticate');
  Route::post('authenticate/facebook', 'AuthenticateController@facebook');
  Route::post('authenticate/twitter', 'AuthenticateController@twitter');

  Route::get('questions/current', 'QuestionController@showCurrent');
  Route::get('questions/today', function() {
     return Redirect::to('/api/v1/questions/current', 301);
  });

  // Everything here requires user authentication
  Route::group(array('middleware' => 'auth'), function() {
    Route::resource('questions', 'QuestionController', ['only' => ['index', 'store', 'update', 'show', 'destroy']]);
    Route::resource('questions.answers', 'AnswerController', ['only' => ['index', 'store', 'update', 'show', 'destroy']]);


    Route::resource('users', 'UserController', ['only' => ['index', 'store', 'update', 'show', 'destroy']]);
    Route::get('users/{users}/score', 'UserController@showScore');
    Route::resource('users.questions', 'UserQuestionController', ['only' => ['index', 'show']]);
    Route::resource('users.questions.answers', 'UserQuestionAnswerController', ['only' => ['index', 'store', 'update', 'destroy']]);

    Route::get('trivia/today', 'TriviaController@showToday');
    Route::resource('trivia', 'TriviaController', ['only' => ['index', 'store', 'update', 'show', 'destroy']]);
  });
});

// Redirect all undefined routes to Angular
Route::any('{undefinedRoute}', function ($undefinedRoute) {
	return File::get(public_path() . '/angular.html');
})->where('undefinedRoute', '([A-z\d-\/_.]+)?');