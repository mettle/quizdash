(function() {
    'use strict';

    angular.module('config.timer', [
        'timer'
    ]);
})();