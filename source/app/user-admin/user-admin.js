(function() {
    'use strict';

    angular
        .module('app.user-admin')
        .controller('UserAdmin', UserAdmin);

    UserAdmin.$inject = ['Users', 'toastr'];

    function UserAdmin(Users, toastr) {

        var vm = this;
        vm.beginEditing = beginEditing;
        vm.collapse = collapse;
        vm.editingCell = editingCell;
        vm.expand = expand;
        vm.finishEditing = finishEditing;
        vm.deleteRow = deleteRow;
        vm.displayAddRecord = displayAddRecord;
        vm.resetAddRecord = resetAddRecord;
        vm.doAddRecord = doAddRecord;
        vm.clearAnswer = clearAnswer;
        vm.toggleSubscribe = toggleSubscribe;
        vm.toggleAdmin = toggleAdmin;
        vm.users = [];

        Users.get().$promise.then(load);

        var editedCol, editedRow;

        function collapse(user) {
            delete user.expand;
            delete user.answers;
        }

        function expand(user) {
            Users.get({u_id: user.id, q_url: 'questions'}).$promise.then(function(data) {
                user.questions = data.user.questions;
                user.expand = true;
            });
        }

        function displayAddRecord() {
          vm.newRecord = {
            admin: false
          };
        }

        function resetAddRecord() {
          vm.newRecord = null;
        }

        function doAddRecord() {
          Users.save({}, vm.newRecord).$promise.then(finalizeAddRecord);
        }

        function finalizeAddRecord(response) {
          if (!response.success) {
            toastr.error('Could not add record', 'Error');
          } else {
            vm.users.push(response.user);
            toastr.success('Saved!', 'Success');
          }
          vm.newRecord = null;
        }

        function beginEditing(row, col) {
            if (!editedRow && !editedCol) {
                editedRow = row.id;
                editedCol = col;
            }
        }

        function editingCell(row, col) {
            return (row.id === editedRow && col === editedCol) ? true : false;
        }

        function finishEditing(user) {
            if (editedRow && editedCol) {
                editedRow = null;
                editedCol = null;
                Users.update({u_id: user.id}, user).$promise.then(finalizeEdit);
            }
        }

        function finalizeEdit(response) {
            if (!response.success) {
                toastr.error('Unable to save record', 'Error');
                vm.users[editedRow-1][editedCol] = vm.editedOriginalValue;
            } else {
                toastr.success('Saved!', 'Success');
            }
            editedRow = null;
            editedCol = null;
        }

        function error(data) {
            console.log('error!');
            console.dir(data);
            //$state.go('anon.landing');
        }

        function deleteRow(user) {
          if (confirm('Are you sure you want to delete this item? This cannot be undone.')) {
            editedRow = user.id;
            Users.delete({u_id: user.id}, user).$promise.then(finalizeDelete);
          }
        }

        function clearAnswer(user, question, answer_id) {
          if (confirm('Are you sure you want to reset this question? This cannot be undone.')) {
            editedRow = user.id;
            Users.deleteAnswer({u_id: user.id, q_id: question.id, a_id: answer_id}).$promise.then(finalizeClearAnswer);
          }
        }

        function finalizeClearAnswer(response) {
          //TODO: Remove the answer from the current array
          toastr.success('Answer removed from user history', 'Success');
        }

        function finalizeDelete(response) {
          if (!response.success) {
            toastr.error('Unable to remove user', 'Error');
          } else {
            // Find and remove the item from the model
            for (var i=0; i<vm.users.length; i++) {
              if (vm.users[i].id == editedRow) {
                vm.users.splice(i, 1);
                break;
              }
            }

            editedRow = null;
            toastr.success('User deleted', 'Success');
          }
        }

        function toggleSubscribe(user) {
          user.subscribe = !user.subscribe;
          Users.update({u_id: user.id}, user).$promise.then(finalizeEdit);
        }

        function toggleAdmin(user) {
          user.admin = !user.admin;
          Users.update({u_id: user.id}, user).$promise.then(finalizeEdit);
        }

        function load(users) {
        	vm.users = users.users;
        }
    }
})();