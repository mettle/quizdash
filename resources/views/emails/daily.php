<html>
    <body>
      <table border="0" width="100%">
        <tr><td bgcolor="#424242"><img src="http://quizdaily.dev/images/logo-static.png" alt="Quiz Daily" title="Quiz Daily" /></td></tr>
        <tr><td>
          <h4>It's time!</h4>
          <p>Your Quiz Daily trivia question of the day is ready! Jump over to QuizDaily.co.uk to see if you know the answer or if we've managed to stump you.</p>
          <p>Take the quiz at <a href="http://quizdaily.co.uk">QuizDaily.co.uk</a>!</p>
        </td></tr>
      </table>
      <small>This email was sent to <?php echo $email; ?>. Don't want daily Quiz Daily reminder emails? No problem! <a href="http://quizdaily.co.uk/contact/unsubscribe?email=<?php echo $email; ?>">Click here to unsubscribe</a>.</small>
    </body>
</html>