(function() {
    'use strict';

    angular
        .module('app.submit')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'user.submit',
                config: {
                    url: '/submit',
                    views: {
                        'content@': {
                            templateUrl: '/views/submit/submit.html',
                            controller: 'Submit',
                            controllerAs: 'vm'
                        },
                        'header@': {
                            templateUrl: '/views/user/header.html',
                            controller: 'Header',
                            controllerAs: 'vm'
                        },
                    }
                }
            }
        ];
    }
})();