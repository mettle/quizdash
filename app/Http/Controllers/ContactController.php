<?php

namespace App\Http\Controllers;

use Mail;
use Config;
use Response;
use Validator;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Ctct\ConstantContact;
use Ctct\Components\Contacts\Contact;
use Ctct\Exceptions\CtctException;

class ContactController extends Controller {

    public function sendContact(Request $request)     {
      Mail::send('emails.contact', ['name' => $request->get('name'), 'email' => $request->get('email'), 'comments' => $request->get('comments')], function ($m) {
          $admin_email = Config::get('app.admin_email');
          $m->to($admin_email)->subject('Quiz Dash Contact');
      });
      return $this->response(null, true);
    }

    public function sendRefer(Request $request)     {
      $emails = $request->get('emails');
      $emails = explode(',', $emails);

      foreach ($emails as $email) {
        $email = trim($email);

        $validator = Validator::make(['email' => $email], [
          'email'       => 'required|email'
        ]);

        if (!$validator->fails()) {
          try {
            Mail::send('emails.refer', ['email' => $email], function ($m) use ($email) {
              $m->to($email)->subject('Play the game all your friends are talking about!');
            });
          } catch (Exception $e) {
            $error = $e;
          }
        }
      }
      return $this->response(null, true);
    }

    public function shareQuestion(Request $request)     {
      $validator = Validator::make(['email' => $request->get('email')], [
        'email'       => 'required|email'
      ]);

      if (!$validator->fails()) {
        $email = $request->get('email');
        $question = $request->get('question');

        try {
          Mail::send('emails.share', ['email' => $email, 'question' => $question], function ($m) use ($email, $question) {
            $m->to($email)->subject('Daily trivia: '.$question['question']);
          });
        } catch (Exception $e) {
          $error = $e;
        }
      }

      return $this->response(null, true);
    }

    public function sendQuestion(Request $request) {
      $username = null;
      $email = null;

      if ($this->authenticatedUser) {
        $username = $this->authenticatedUser->username;
        $email = $this->authenticatedUser->email;
      }
      Mail::send('emails.submit', ['username' => $username, 'email' => $email, 'question' => $request->get('question'), 'answer' => $request->get('answer'), 'name' => $request->get('name'), 'hometown' => $request->get('hometown')], function ($m) {
          $admin_email = Config::get('app.admin_email');
          $m->to($admin_email)->subject('Quiz Dash: User submitted question');
      });
      return $this->response(null, true);
    }

    public function unsubscribe(Request $request) {
      $user = User::where('email', $request->get('email'))->update(['subscribe' => false]);


      $cc_key = Config::get('app.constantcontact_api_key');
      $cc_access_token = Config::get('app.constantcontact_access_token');
      $cc_list_id = Config::get('app.constantcontact_list_id');
      $cc = new ConstantContact($cc_key);

      $response = $cc->contactService->getContacts($cc_access_token, array("email" => $request->get('email')));
      if (!empty($response->results)) {
          $contact = $response->results[0];
          if ($contact instanceof Contact) {
            $contact->lists = [];

            $returnContact = $cc->contactService->updateContact($cc_access_token, $contact);
          }
      }


      return $request->get("email") ." has been unsubscribed from daily quiz reminders";
    }
}
