(function() {
    'use strict';

    angular
        .module('app.user')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'user',
                config: {
                    url: '/answer',
                    abstract: true,
                    views: {
                        'header@': {
                            templateUrl: '/views/user/header.html',
                            controller: 'Header',
                            controllerAs: 'vm'
                        },
                        'footer@': {
                            templateUrl: '/views/footer/footer.html'
                        },
                        'notice@': {
                            templateUrl: '/views/notice/cookies.html',
                            controller: 'Notice',
                            controllerAs: 'vm'
                        }
                    },
                    data: {
                        requireLogin: true
                    }
                }
            }
        ];
    }
})();