<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use GuzzleHttp;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Hash;
use Config;
use JWT;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Ctct\ConstantContact;
use Ctct\Components\Contacts\Contact;
use Ctct\Exceptions\CtctException;

class AuthenticateController extends Controller {

  public function authenticate(Request $request) {
    $credentials = $request->only('email', 'password');
    // Check if the email is in the system
    $user = User::with('questions')->where('email', $request->get('email'))->get();

    if (count($user)>0) {
      $user = $user->first();
      if (Hash::check($credentials['password'], $user->password)) {
          $ret = ['token' => $this->createToken($user)];

          // Load user and questions
          $ret['user'] = $user;

          if (strlen($ret['user']->avatar)<=0) {
              $ret['user']->avatar = 'http://www.gravatar.com/avatar/'.md5($request->email).'/?s=50&d=http://quizdash.co.uk/images/avatar-quizmaster.png';
          }

          $score = [
            'answered' => 0,
            'correct'  => 0,
            'percent' => 0
          ];

          $questions = $ret['user']->questions->each(function($question) use (&$score) {
            $score['answered']++;
            if ($question->correctAnswer['id'] == $question->pivot['answer_id']) {
              $score['correct']++;
            }
          });

          // Calculate the percent
          if ($score['answered']>0) $score['percent'] = round($score['correct']/$score['answered']*100);
          // We don't want to return the questions, so we'll unset them
          unset($ret['user']->questions);

          $ret['user']['score'] = $score;
      } else {
          return $this->response(['errors' => ['Wrong email and/or password']], false, 401);
      }

      return $this->response($ret, true);
    } else {
      //! User doesn't exist. Create them.

      // Save email to Constant Contact
      $cc_key = Config::get('app.constantcontact_api_key');
      $cc_access_token = Config::get('app.constantcontact_access_token');
      $cc_list_id = Config::get('app.constantcontact_list_id');
      $cc = new ConstantContact($cc_key);

      $response = $cc->contactService->getContacts($cc_access_token, array("email" => $request->get('email')));
      // create a new contact if one does not exist
      if (empty($response->results)) {
          $contact = new Contact();
          $contact->addEmail($request->get('email'));
          $contact->addList(strval($cc_list_id));

          $returnContact = $cc->contactService->addContact($cc_access_token, $contact);
      }

      // Determine username
      $username = substr($request->get('email'), 0, strpos($request->get('email'), '@'));
      $username = preg_replace("/[^A-Za-z0-9]/", '', $username);
      while (true) {
        $dupe = User::with('questions')->where('username', $username)->get();
        if (count($dupe)>0) {
          $username .= '1';
        } else {
          break;
        }
      }

      $user = new User;
      $user->username   = $username;
      $user->email      = $request->email;
      $user->admin      = false;
      $user->password   = Hash::make($request->get('password'));
      $user->ip         = $request->ip();
      $user->avatar     = 'http://www.gravatar.com/avatar/'.md5($request->email).'/?s=50';

      $user->save();

      $ret = ['token' => $this->createToken($user), 'user' => $user, 'newUser' => true];
      return $this->response($ret, true);
    }
  }

  public function facebook(Request $request) {
      $accessTokenUrl = 'https://graph.facebook.com/v2.3/oauth/access_token';
      $graphApiUrl = 'https://graph.facebook.com/v2.3/me';
      $params = [
          'code' => $request->get('code'),
          'client_id' => $request->get('clientId'),
          'redirect_uri' => $request->get('redirectUri'),
          'client_secret' => Config::get('app.facebook_secret')
      ];
      $client = new GuzzleHttp\Client();
      //! Step 1. Exchange authorization code for access token.
      $accessToken = $client->get($accessTokenUrl, ['query' => $params])->json();
      //! Step 2. Retrieve profile information about the current user.
      $profile = $client->get($graphApiUrl, ['query' => $accessToken])->json();

      // Get profile image
      $avatarUrl = $graphApiUrl.'/picture?redirect=false';
      $avatarData = $client->get($avatarUrl, ['query' => $accessToken])->json();

      if ($request->header('Authorization')) {
          //! Step 3a. If user is already signed in then link accounts.
          $user = User::with('questions')->where('facebook', '=', $profile['id']);
          if ($user->first()) {
              return response()->json(['message' => 'There is already a Facebook account that belongs to you'], 409);
          }
          $token = explode(' ', $request->header('Authorization'))[1];
          $payload = (array) JWT::decode($token, Config::get('app.token_secret'), array('HS256'));
          $user = User::with('questions')->find($payload['sub']);
          $user->facebook = $profile['id'];
          $user->username = $user->username  || $profile['name'];
          if ($avatarData['data']['url']) {
            $user->avatar = $avatarData['data']['url'];
          }
          $user->save();

          $ret = ['token' => $this->createToken($user)];

      } else {
          //! Step 3b. Create a new user account or return an existing one.
          $user = User::with('questions')->where('facebook', '=', $profile['id']);

          if ($user->first()) {
            //! Step 3b1. Login to existing account
            $user = $user->first();
            if ($avatarData['data']['url']) {
              // update avatar
              $user->avatar = $avatarData['data']['url'];
              $user->save();
            }

            $ret = ['token' => $this->createToken($user)];
          } else {
            //! Step 3b2. Create new account
            $user = new User;
            $user->facebook = $profile['id'];
            $user->username = $profile['name'];
            if ($avatarData['data']['url']) {
              $user->avatar = $avatarData['data']['url'];
            }
            $user->save();

            $ret = ['token' => $this->createToken($user)];
          }
      }

      // Load user and questions
      if ($user) {
        $ret['user'] = $user;
        if ($avatarData['data']['url']) {
          $ret['user']->avatar = $avatarData['data']['url'];
        }
        $score = [
          'answered' => 0,
          'correct'  => 0,
          'percent' => 0
        ];

        $questions = $ret['user']->questions->each(function($question) use (&$score) {
          $score['answered']++;
          if ($question->correctAnswer['id'] == $question->pivot['answer_id']) {
            $score['correct']++;
          }
        });

        // Calculate the percent
        if ($score['answered']>0) $score['percent'] = round($score['correct']/$score['answered']*100);
        // We don't want to return the questions, so we'll unset them
        unset($ret['user']->questions);

        $ret['user']['score'] = $score;
      }

      return $this->response($ret, true);
  }

  public function twitter(Request $request) {
      $requestTokenUrl = 'https://api.twitter.com/oauth/request_token';
      $accessTokenUrl = 'https://api.twitter.com/oauth/access_token';
      $profileUrl = 'https://api.twitter.com/1.1/users/show.json?screen_name=';

      $client = new GuzzleHttp\Client();

      if (!$request->get('oauth_token') || !$request->get('oauth_verifier')) {
          //! Part 1 of 2: Initial request from Satellizer.
          $requestTokenOauth = new Oauth1([
            'consumer_key' => Config::get('app.twitter_key'),
            'consumer_secret' => Config::get('app.twitter_secret'),
            'callback' => Config::get('app.twitter_callback')
          ]);

          $client->getEmitter()->attach($requestTokenOauth);

          //! Step 1. Obtain request token for the authorization popup.
          $requestTokenResponse = $client->post($requestTokenUrl, ['auth' => 'oauth']);

          $oauthToken = array();
          parse_str($requestTokenResponse->getBody(), $oauthToken);

          //! Step 2. Send OAuth token back to open the authorization screen.
          return response()->json($oauthToken);

      } else {
          //! Part 2 of 2: Second request after Authorize app is clicked.
          $accessTokenOauth = new Oauth1([
              'consumer_key' => Config::get('app.twitter_key'),
              'consumer_secret' => Config::get('app.twitter_secret'),
              'token' => $request->get('oauth_token'),
              'verifier' => $request->get('oauth_verifier')
          ]);

          $client->getEmitter()->attach($accessTokenOauth);

          //! Step 3. Exchange oauth token and oauth verifier for access token.
          $accessTokenResponse = $client->post($accessTokenUrl, ['auth' => 'oauth'])->getBody();

          $accessToken = array();
          parse_str($accessTokenResponse, $accessToken);

          $profileOauth = new Oauth1([
              'consumer_key' => Config::get('app.twitter_key'),
              'consumer_secret' => Config::get('app.twitter_secret'),
              'oauth_token' => $accessToken['oauth_token']
          ]);

          $client->getEmitter()->attach($profileOauth);

          //! Step 4. Retrieve profile information about the current user.
          $profile = $client->get($profileUrl . $accessToken['screen_name'], ['auth' => 'oauth'])->json();

          if ($request->header('Authorization')) {
              //! Step 5a. Link user accounts.
              $user = User::where('twitter', '=', $profile['id']);
              if ($user->first()) {
                  return response()->json(['message' => 'There is already a Twitter account that belongs to you'], 409);
              }

              $token = explode(' ', $request->header('Authorization'))[1];
              $payload = (array) JWT::decode($token, Config::get('app.token_secret'), array('HS256'));

              $user = User::find($payload['sub']);
              $user->twitter = $profile['id'];
              $user->username = $user->username || '@'.$profile['screen_name'];
              $user->avatar = $profile['profile_image_url'];
              $user->save();

              $ret = ['token' => $this->createToken($user)];
          } else {
              //! Step 5b. Create a new user account or return an existing one.
              $user = User::where('twitter', '=', $profile['id']);

              if ($user->first()) {
                  $user = $user->first();
                  $ret = ['token' => $this->createToken($user->first())];
              } else {
                  $user = new User;
                  $user->twitter = $profile['id'];
                  $user->username = '@'.$profile['screen_name'];
                  $user->avatar = $profile['profile_image_url'];
                  $user->save();

                  $ret = ['token' => $this->createToken($user)];
              }
          }

          if ($user) {
            $ret['user'] = $user;
            $ret['user']->avatar = $profile['profile_image_url'];
            $score = [
              'answered' => 0,
              'correct'  => 0,
              'percent' => 0
            ];

            $questions = $ret['user']->questions->each(function($question) use (&$score) {
              $score['answered']++;
              if ($question->correctAnswer['id'] == $question->pivot['answer_id']) {
                $score['correct']++;
              }
            });

            // Calculate the percent
            if ($score['answered']>0) $score['percent'] = round($score['correct']/$score['answered']*100);
            // We don't want to return the questions, so we'll unset them
            unset($ret['user']->questions);

            $ret['user']['score'] = $score;
          }

          return $this->response($ret, true);

      }
  }

  protected function createToken($user) {
      $payload = [
          'sub' => $user->id,
          'iat' => time(),
          'exp' => time() + (2 * 7 * 24 * 60 * 60)
      ];
      return JWT::encode($payload, Config::get('app.token_secret'));
  }
}
