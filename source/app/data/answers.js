(function() {
    'use strict';

    angular
        .module('app.data')
        .factory('Answers', Answers);

    Answers.$inject = ['$resource'];

    function Answers($resource) {
    	return $resource('/api/v1/questions/:q_id/answers/:a_id', {q_id: this.q_id, a_id: this.a_id}, {
            update: {method: 'PUT'}
        });
    }
})();