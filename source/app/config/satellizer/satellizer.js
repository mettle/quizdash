(function() {
    'use strict';

    angular
        .module('config.satellizer')
        .config(Satellizer);

    Satellizer.$inject = ['$authProvider'];

    function Satellizer($authProvider) {
        $authProvider.loginUrl = '/api/v1/authenticate';
        $authProvider.tokenPrefix = '';
        $authProvider.facebook({
          clientId: '1475848056054614',
          url: '/api/v1/authenticate/facebook'
        });
        $authProvider.twitter({
          url: '/api/v1/authenticate/twitter'
        });
    }
})();