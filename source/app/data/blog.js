(function() {
    'use strict';

    angular
        .module('app.data')
        .factory('Blog', Blog);

    Blog.$inject = ['$resource'];

    function Blog($resource) {
        return $resource('/blog/index.php/api/:action', {action: this.action}, {
            getPosts: {method: 'GET', params: {action: 'get_posts'}}
        });
    }
})();