(function() {
    'use strict';

    angular
        .module('app.directives')
        .directive('chikitaAd', ChikitaAd);

//    ChikitaAd.$inject = [$scope];

    function ChikitaAd() {
        var directive = {
            restrict: 'A',
            replace: true,
            template: function(elem, attr) {

                var width = 550;
                var height = 250;

                switch (attr.adType) {
                    case 'leaderboard':
                        width = 728;
                        height = 90;
                        break;
                    case 'wide-skyscraper':
                        width = 160;
                        height = 600;
                }

                if (window.CHITIKA === undefined) { window.CHITIKA = { 'units' : [] }; }
                var unit = {"calltype":"async[2]","publisher":"bighugemedia","width":width,"height":height,"sid":"Chitika Default","color_site_link":"03cec2","color_button":"03cec2","color_button_text":"FFFFFF"};
                var placement_id = window.CHITIKA.units.length;
                window.CHITIKA.units.push(unit);
                return '<div id="chitikaAdBlock-' + placement_id + '"></div>';
            }
        };
        return directive;
    }
})();

