(function() {
    'use strict';

    angular
        .module('app.profile')
        .controller('Profile', Profile);

    Profile.$inject = ['Users', 'UserStore', 'toastr'];

    function Profile(Users, UserStore, toastr) {

        var vm = this;
        vm.user = UserStore.getUser();
        vm.doSave = doSave;
        vm.style = "white";

        function doSave() {
          Users.update({u_id: vm.user.id }, vm.user).$promise.then(finalizeSave);
        }

        function finalizeSave(response) {
          if (!response.success) {
            console.log('Could not save');
          } else {
            toastr.success('Saved!', 'Success');
            console.log('Saved');
          }
        }


    }
})();