<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {
  use SoftDeletes, Authenticatable, CanResetPassword;

  public function questions() {
    $questions = $this->belongsToMany('App\Question')->withTimestamps()->withPivot('answer_id')->with('correctAnswer');

    return $questions;
  }

  public function answered() {
    return count($this->questions());
  }

  public function getScore() {
    $score = [
      'answered' => 0,
      'correct'  => 0,
      'percent' => 0
    ];

    $questions = $this->questions->each(function($question) use (&$score) {
      $score['answered']++;
      if ($question->correctAnswer['id'] == $question->pivot['answer_id']) {
        $score['correct']++;
      }
    });

    // Calculate the percent
    if ($score['answered']>0) $score['percent'] = round($score['correct']/$score['answered']*100);

    return $score;
  }

	protected $hidden = ['password', 'remember_token', 'accesstoken'];
  protected $dates  = ['deleted_at'];
}
