(function() {
    'use strict';

    angular
        .module('app.question-admin')
        .run(appRun);

    function appRun(routeHelper) {
        routeHelper.configureStates(getStates());
    }

    function getStates() {
        return [
            {
                state: 'admin.questions',
                config: {
                    url: '/admin/questions',
                    views: {
                        'content@': {
                            templateUrl: '/views/question-admin/question-admin.html',
                            controller: 'QuestionAdmin',
                            controllerAs: 'vm'
                        }
                    }
                }
            }
        ];
    }
})();