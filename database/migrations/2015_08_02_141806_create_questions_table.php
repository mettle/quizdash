<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration {
  public function up() {
    Schema::create('questions', function (Blueprint $table) {
      $table->increments('id');
      $table->string('question', 255);
      $table->text('detail');
      $table->string('image', 50)->nullable();
      $table->datetime('scheduled')->nullable();
      $table->datetime('posted')->nullable();
      $table->timestamps();
    });
  }

  public function down() {
    Schema::drop('questions');
  }
}
